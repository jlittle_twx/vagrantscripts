#!/bin/sh

# purge.sh - Cleans up the empty space on the VM prior to packaging a Vagrant Box.

# must be run as root.
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo "Cleaning apt repository..."
apt-get -y autoremove
apt-get clean
apt-get autoclean

echo "Cleaning bash history..."
unset HISTFILE
[ -f /root/.bash_history ] && rm /root/.bash_history
[ -f /home/vagrant/.bash_history ] && rm /home/vagrant/.bash_history

echo "Cleaning up log files..."
find /var/log -type f | while read f; do echo -ne '' > $f; done

echo "Zeroing out free space..."
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY
