#!/bin/bash
cd /home/vagrant/scripts

if git diff-index --quiet HEAD --; then
	echo "Updating from repository."
	git pull
else
	echo "Changes detected, forcing update from repository."
	git reset --hard && git pull
fi
