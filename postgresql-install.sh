#!/bin/sh
# postgresql-install.sh <version>
#
# Downloads, installs and configures PostgreSQL for external connections on Ubuntu or RedHat based OSes.
# Does not currently handle HA configuration.
# Tested with CentOS 7 and Ubuntu 14.04 LTS (9.2 and 9.4 versions)

# Script takes one parameter
if [ $# -eq 1 ]; then

	# Currently only supporting PostgreSQL 9.2, 9.3 and 9.4
	if echo $1 | grep -vEq "[9]\.[2-4]"; then 
		echo "Usage: postgresql-setup.sh <version>"
		echo "  where <version> is one of 9.2, 9.3, 9.4"
		exit 1
	fi
else
	echo $1
	echo "Usage: postgresql-setup.sh <version>"
	echo "  where <version> is one of 9.2, 9.3, 9.4"
	exit 1
fi

# Determine distro
if cat /proc/version | grep -qi "Ubuntu"; then
	echo "Installing postgres for Ubuntu..."
	UBUNTU=1
elif cat /proc/version | grep -qi "Red Hat"; then
	if cat /etc/redhat-release | grep -qi "CentOS"; then 
		echo "Installing postgres for CentOS"
		CENTOS=1
	else
		echo "Installing postgres for Red Hat"
		REDHAT=1
	fi
fi

if [ "$UBUNTU" = "1" ]; then 
	if ! dpkg -l "postgresql-$1" >/dev/null; then
		echo "PostgreSQL not found.  Installing."

		# Get the codename for the ubuntu release. This is needed for the
		# PostgreSQL Repository
		CODENAME=`lsb_release -c | cut -f2`

		# Add the APT repository (9.x) if necessary
		if ! grep -q "$CODENAME-pgdg" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
			echo "  ... Adding APT Repository"
			sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -c | cut -f2`-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
			wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
			sudo apt-get update 
		fi
	
		# Install the selected version of postgresql
		sudo apt-get install -y postgresql-$1 postgresql-contrib-$1

		# Setup the password for the postgres default user
		sudo -u postgres psql postgres -c "ALTER USER postgres WITH PASSWORD 'postgres';"
				
	fi
	
	PGHBA_LOC="/etc/postgresql/$1/main/pg_hba.conf"
	PGCONF_LOC="/etc/postgresql/$1/main/postgresql.conf"	
	
elif [ "$REDHAT" = "1" -o "$CENTOS" = "1" ]; then

	NODOT=`echo $1 | tr -d '.'`

	# Set up the yum repository
	if [ "$CENTOS" = "1" ]; then 
		# What is the difference between -1 and -2? Just newer?
		sudo rpm -Uh http://yum.postgresql.org/$1/redhat/rhel-7-x86_64/pgdg-centos$NODOT-$1-2.noarch.rpm
	elif [ "$REDHAT" = "1" ]; then
		sudo rpm -Uh http://yum.postgresql.org/$1/redhat/rhel-7-x86_64/pgdg-redhat$NODOT-$1-2.noarch.rpm
	fi
	
	# Install postgresql
	sudo yum install -y postgresql$NODOT postgresql$NODOT-server postgresql$NODOT-contrib
	
	# Initialize the database
	sudo /usr/pgsql-$1/bin/postgresql$NODOT-setup initdb
	
	# Enable startup at boot
	sudo chkconfig postgresql-$1 on
	
	# Startup postgres
	sudo service postgresql-$1 start
	
	# Setup the password for the postgres default user
	sudo su -l postgres -c "psql postgres -c \"ALTER USER postgres WITH PASSWORD 'postgres';\""
	
	# Set up configuration files.
	sudo cp /usr/pgsql-$1/share/pg_hba.conf.sample /usr/pgsql-$1/share/pg_hba.conf
	sudo cp /usr/pgsql-$1/share/postgresql.conf.sample /usr/pgsql-$1/share/postgresql.conf
	
	PGHBA_LOC="/usr/pgsql-$1/share/pg_hba.conf"
	PGCONF_LOC="/usr/pgsql-$1/share/postgresql.conf"
	
fi

# Configuration
echo "Configuring PostgreSQL"
# Configure the postgres server to accept IP connections.
# Change peer authentication for local connections to md5.
if sudo grep -Eq "local.*all.*postgres.*peer" "$PGHBA_LOC"; then
	echo "  ... Enabling local connections"
	sudo sed -i "s/\(^local.*all.*postgres.*\)peer.*/\1 md5/" "$PGHBA_LOC"
fi
	
# Enable IPV4 connections
if ! sudo grep -q "0.0.0.0/0" "$PGHBA_LOC"; then
	echo "  ... Enabling IPV4 Connections"
	echo "# External IPV4 connections" | sudo tee -a "$PGHBA_LOC" >/dev/null
	echo "host    all             all             0.0.0.0/0                md5" | sudo tee -a "$PGHBA_LOC" >/dev/null
fi
	
# Enable IPV6 connections
if ! sudo grep -q "::0/0" "$PGHBA_LOC"; then 
	echo "  ... Enabling IPV6 Connections"
	echo "# External IPV6 connections" | sudo tee -a "$PGHBA_LOC" >/dev/null
	echo "host    all             all                 ::0/0                md5" | sudo tee -a "$PGHBA_LOC" >/dev/null
fi
	
# Listen for connections from all IP Addresses
if ! grep -q "listen_addresses = '\*'" "$PGCONF_LOC"; then
	echo "  ... Telling PostgreSQL to respond to all addresses."
	sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PGCONF_LOC"
fi

# Restart the service as it's likely that some parameters have been changed.
if [ "$UBUNTU" = "1" ]; then 	
	sudo service postgresql restart
elif [ "$CENTOS" = "1" -o "$REDHAT" = "1" ]; then
	sudo service postgresql-$1 restart
fi