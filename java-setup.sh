#!/bin/sh
# Installs Oracle JDK 7,8 on a clean Linux Platform.  
# (Does not switch between multiple JDKs on the same machine at this time.)
#
# Usage: ./java-setup.sh <7|8>
# Tested with Ubuntu 14.04 with Oracle Java 8.

if [ $# -eq 1 ]; then

	if [ $1 -lt 7 -o $1 -gt 8 ]; then
		echo "java-setup.sh - expecting 6, 7 or 8 as parameter."
		exit 1;
	fi

	# Determine distro
	if cat /proc/version | grep -qi "Ubuntu"; then
		echo "  ... Installing Java for Ubuntu..."
		UBUNTU=1
	elif cat /proc/version | grep -qi "Red Hat"; then
		if cat /etc/redhat-release | grep -qi "CentOS"; then 
			echo "  ... Installing Java for CentOS"
			CENTOS=1
		else
			echo "  ... Installing Java for Red Hat"
			REDHAT=1
		fi
	fi

	if [ "$UBUNTU" = "1" ]; then 
		# Add the apt repository that provides oracle installers.
		echo "  ... Adding APT Repository for Java updates"
		sudo add-apt-repository -y ppa:webupd8team/java
		sudo apt-get update -qq

		if ! dpkg -l oracle-java$1-installer 2>&1 > /dev/null; then
			# This will install without license prompt
			echo "  ... Installing Oracle Java $1"
			echo oracle-java$1-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
			sudo apt-get install -y -qq oracle-java$1-installer >/dev/null
			sudo apt-get install -y -qq oracle-java$1-set-default >/dev/null
		fi
	elif [ "$CENTOS" = "1" ]; then

		# Have to hard code version strings.  There's no rhyme or reason to the way Oracle names releases.
		if [ $1 -eq 7 ]; then 
			VER_FULL="7u80-b15"
			PKG_NAME="jdk-1.7.0_80.x86_64"
		elif [ $1 -eq 8 ]; then 
			VER_FULL="8u66-b17"
			PKG_NAME="jdk-1.8.0_66.x86_64"
		fi
		
		if ! yum list installed $PKG_NAME; then
			echo "  ... Oracle JDK is not installed.  Installing."
		
			# Retrieve the RPM file for the JDK installation. Cookie is added to automatically accept licence agreement.
			wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/$VER_FULL/jdk-`echo $VER_FULL | cut -d- -f1`-linux-x64.rpm"
	
			# Install the RPM file
			sudo yum localinstall -y jdk-`echo $VER_FULL | cut -d- -f1`-linux-x64.rpm 
	
			rm jdk-*-linux-x64.rpm
		fi
	fi

else

	echo "Usage:"
	echo "  java-setup.sh [version]"
	echo ""
	echo "    where [version] is one of 7, 8"
	exit 1

fi
