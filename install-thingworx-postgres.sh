#!/bin/bash
# Usage: ./install-thingworx-postgres.sh [-t <tomcatVersion>] [-p <postgresqlVersion>] [-j <javaVersion>]
#   -x Thingworx Version (X.Y.Z) - Default '6.6.0'
#		OR
#	-f Zip file containing ThingWorx release to install.
# 	-t Tomcat Version - Either X.0.Y or X for latest from major version X. Default '8'
#   -p Postgres Version - Default '9.4'
#	-u eSupport Portal Username
#	-v eSupport Portal Password (will be encrypted)
#	-l License activation key (8.1)


# Default values
TOMCATVER="8"
POSTGRESVER="9.4"
TWXVER="6.6.0"
SCRIPTSDIR="/home/vagrant/scripts"

# Constants
TWXTEMP="TWXExtract"
TOMCATUSR="tomcat8"
TOMCATDIR="/opt/tomcat8"

# Process flags as necessary.
while [ "$1" != "" ]; do
	case $1 in
		-x | -X )	shift
					TWXVER="$1"
					if [ -z "$SRC" ]; then 
						SRC="X"
					else
						echo "ERROR - Cannot specify both the -x and -f flags at the same time!"
						exit 1
					fi
					;;
		-t | -T )	shift
					TOMCATVER="$1"
					;;
		-p | -P )	shift
					POSTGRESVER="$1"
					;;
		-f | -F )	shift
					TWXFILE="$1"
					if [ -z "$SRC" ]; then 
						SRC="F"
					else
						echo "ERROR - Cannot specify both the -x and -f flags at the same time!"
						exit 1
					fi
					;;
		-u | -U )	shift
					SUPTUSER="$1"
					;;
		-v | -V )	shift
					SUPTPASS="$1"
					;;
		-l | -L )	shift
					ACTVID="$1"
					;;
		--help )	shift
					echo "usage: install-thingworx-postgres.sh [-t <tomcatVersion>] [-p <postgresqlVersion>] [-x <ThingWorxVersion> | -f <localThingWorxZip>] [-u eSupport username] [-v eSupport Password] [-l TWX 8.1+ Activation ID]"
					exit 1
					;;
		* )
					echo "Unknown Option $1"
					exit 1
					;;
	esac
	shift
done

echo "Tomcat: $TOMCATVER, Postgres: $POSTGRESVER, Thingworx: $TWXVER"

## VALIDATION

# Thingworx version - Only 6.5+ supported
# For futureproofing, I'm only checking old versions.  New versions will attempt to download and fail if not available.
if [ `echo "$TWXVER" | cut -f1 -d.` -lt 6 ]; then 
	echo "Script only supports Thingworx 6.5 or higher. Thingworx Version provided: $TWXVER"
	exit 1
elif [ `echo "$TWXVER" | cut -f1 -d.` -eq 6 -a `echo "$TWXVER" | cut -f2 -d.` -lt 5 ]; then 
	echo "Script only supports Thingworx 6.5 or higher. Thingworx Version provided: $TWXVER"
	exit 1
fi

# Postgres Version - Only 9.4-9.5 currently supported.
if [ "$POSTGRESVER" != "9.4" -a "$POSTGRESVER" != "9.5" ]; then 
	echo "Script only supports Postgres 9.4 or 9.5. Postgres Version provided: $POSTGRESVER"
	exit 1
fi

# Tomcat Version - Only 7 and 8 are supported.  Not checking minors.
if [ `echo "$TOMCATVER" | cut -f1 -d.` -ne 7 -a `echo "$TOMCATVER" | cut -f1 -d.` -ne 8 ]; then 
	echo "Script only supports Tomcat 7 or 8. Tomcat Version provided: $TOMCATVER"
	exit 1
fi

# Install Additional Packages if necessary
if ! dpkg -l unzip 2>&1 >/dev/null; then
	echo "  ... Installing unzip."
	sudo apt-get install -qy unzip >/dev/null
fi

if ! dpkg -l wget 2>&1 >/dev/null; then
	echo "  ... Installing wget."
	sudo apt-get install -qy wget >/dev/null
fi

# Download Thingworx version
TWXMAJ=`echo $TWXVER | cut -f1 -d.`
TWXMIN=`echo $TWXVER | cut -f2 -d.`
TWXSP=`echo $TWXVER | cut -f3 -d.`

# eSupport Portal Information check
if [ $TWXMAJ -ge 8 -a $TWXMIN -ge 1 ]; then
	# ThingWorx 8.1 requires username and password for eSupport Portal to retrieve license.
	if [ -z "$SUPTUSER" -o -z "$SUPTPASS" ]; then 
		echo "ERROR: ThingWorx 8.1 requires a PTC username (-u) and password (-v) to be specified for licensing."
		exit 1
	fi
	
	# Pull in default activation ID if available
	if [ -z "$ACTVID" -a -e "/home/vagrant/licenses/activation.sh" ]; then 
		source /home/vagrant/licenses/activation.sh
	fi
	
	# Make sure an activation ID is specified
	if [ -z "$ACTVID" ]; then
		echo "ERROR: Activation ID is not specified.  Cannot continue!"
		exit 1
	fi 	
fi

if [ -z "$TWXFILE" ]; then 
	echo "Downloading ThingWorx Platform $TWXVER for PostgreSQL"

	# Generate the filename for the download based on PTC's archaic naming format.
	MEDID="MED-61111"
	
	# Service Pack Check
	if [ $TWXSP -eq 0 ]; then 
		# If no SP release, then we need to replace _SPx_ with _F000_
		TWXFILE="${MEDID}-CD-0${TWXMAJ}${TWXMIN}_F000_ThingWorx-Platform-Postgres-${TWXMAJ}-${TWXMIN}-${TWXSP}.zip"
	else 	
		TWXFILE="${MEDID}-CD-0${TWXMAJ}${TWXMIN}_SP${TWXSP}_ThingWorx-Platform-Postgres-${TWXMAJ}-${TWXMIN}-${TWXSP}.zip"
	fi

	# Generate the URL
	URL="https://esd.ptc.com/files/TWX/$TWXFILE"
	
	if ! wget --quiet --spider --no-check-certificate $URL > /dev/null; then
		echo "  ...unable to find $TWXFILE.  Trying with 'Thingworx' instead"
		URL=${URL/ThingWorx/Thingworx}
		TWXFILE=${TWXFILE/ThingWorx/Thingworx}

		if ! wget --quiet --spider --no-check-certificate $URL > /dev/null; then
			echo "ERROR: Unable to download ThingWorx $TWXVER from $URL!"
			exit 1
		fi
	fi

	if [ -e "./$TWXFILE" ]; then
		# Thingworx file appears to have already been downloaded.
		echo "  ... Found $TWXFILE in local directory. Skipping download."
	else 	
		# Download Thingworx from PTC, as spider check should have already been done
		echo "  ... Downloading Thingworx $TWXVER from PTC."
		wget --no-check-certificate $URL > /dev/null	
	fi
fi
	
# Extract contents of zip
echo "Extracting ThingWorx to a Temporary Location"
if [ -d "$TWXTEMP" ]; then
	# Directory already exists - Remove and re-extract in case files have been removed/moved.	
	
	# Don't try to delete anything if $TWXTEMP variable is empty! (This could be very bad...)
	if [ ! -z "$TWXTEMP" ]; then 
		echo "  ... Temporary directory already exists. Removing existing files. "
		rm -rf "$TWXTEMP"
	fi
fi

# Extract the Thingworx download into a temporary directory.
echo "  ... Extracting $TWXFILE to temporary directory."
unzip "./$TWXFILE" -d "$TWXTEMP" > /dev/null

## PRE-REQUISITES ##

# Tomcat
# TODO: Script exits with code 1 if it finds an existing install
# Need better way to detect and skip installation.
echo "Installing Tomcat $TOMCATVER"
if [ -e "./tomcat-install.sh" ]; then
	if ! ./tomcat-install.sh $TOMCATVER; then
		echo "ERROR: Unable to install Tomcat!"
	fi
else 
	if ! $SCRIPTSDIR/tomcat-install.sh $TOMCATVER; then
		echo "ERROR: Unable to install Tomcat!"
	fi
fi


# Postgres
echo "Installing PostgreSQL $POSTGRESVER"
if [ -e "./postgresql-install.sh" ]; then
	if ! ./postgresql-install.sh $POSTGRESVER; then 
		echo "ERROR: Unable to install PostgreSQL! Exiting"
		exit 1
	fi
else 
	if ! $SCRIPTSDIR/postgresql-install.sh $POSTGRESVER; then 
		echo "ERROR: Unable to install PostgreSQL! Exiting"
		exit 1
	fi
fi


## SET UP POSTGRES ##
# Create a .pgpass file so we don't get prompted for passwords.
echo "*:*:*:postgres:postgres" > ~/.pgpass
echo "*:*:*:twadmin:twadmin" >> ~/.pgpass
sudo chmod 600 ~/.pgpass

# Create 'twadmin' user in postgres
if ! psql -U postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='twadmin'" | grep -q "1"; then 
	echo "  ... Creating twadmin user in PostgreSQL."
	psql -U postgres -c "CREATE USER twadmin WITH PASSWORD 'twadmin';"
fi

# Create /ThingworxPostgresqlStorage directory and set ownership to 'postgres'
if [ ! -d "/ThingworxPostgresqlStorage" ]; then
	sudo mkdir /ThingworxPostgresqlStorage
	sudo chown postgres:postgres /ThingworxPostgresqlStorage
fi

# Run thingworxPostgresqlDBSetup.sh script
if ! psql -U postgres -lqt | cut -d \| -f1 | grep -w "thingworx"; then 
	CWD=`pwd`
	
	# Thingworx 6.5 and 6.6 have different directory structures.
	if [ -d $TWXTEMP/install ]; then
		cd $TWXTEMP/install/
	else
		cd $TWXTEMP
	fi
	
	echo "  ... Running thingworxPostgresDBSetup Script"
	if ! ./thingworxPostgresDBSetup.sh; then 
		echo "ERROR: Unable to run thingworxPostgresqlDBSetup Script! Exiting."
		exit 1
	fi

	echo "  ... Running thingworxPostgresSchemaSetup"
	if ! ./thingworxPostgresSchemaSetup.sh; then 
		echo "ERROR: Unable to run thingworxPostgresqlSchemaSetup Script! Exiting."
		exit 1
	fi
	
	cd $CWD	
fi

## INSTALL THINGWORX ##
# Create /ThingworxPlatform directory and set ownership to Tomcat
if [ ! -d "/ThingworxPlatform" ]; then 
	echo "  ... Creating /ThingworxPlatform directory"
	sudo mkdir /ThingworxPlatform
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform
fi

# Create /ThingworxStorage directory and set ownership to Tomcat
if [ ! -d "/ThingworxStorage" ]; then 
	echo "  ... Creating /ThingworxStorage directory"
	sudo mkdir /ThingworxStorage
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxStorage
fi

# Create /ThingworxBackupStorage directory and set ownership to Tomcat
if [ ! -d "/ThingworxBackupStorage" ]; then 
	echo "  ... Creating /ThingworxBackupStorage directory"
	sudo mkdir /ThingworxBackupStorage
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxBackupStorage
fi

# Copy Thingworx.war to Tomcat webapps directory
if [ ! -e "$TOMCATDIR/webapps/Thingworx.war" ]; then 
	echo "  ... copying Thingworx.war"
	sudo cp $TWXTEMP/Thingworx.war $TOMCATDIR/webapps
	sudo chown $TOMCATUSR:$TOMCATUSR $TOMCATDIR/webapps/Thingworx.war
fi

# Copy modelproviderconfig.json file from download to /ThingworxPlatform and update settings as necessary.
if [ -e $TWXTEMP/modelproviderconfig.json ]; then 
	if [ ! -e /ThingworxPlatform/modelproviderconfig.json ]; then 
		echo "  ... Copying modelproviderconfig.json to /ThingworxPlatform"
		sudo cp $TWXTEMP/modelproviderconfig.json /ThingworxPlatform
		sudo sed -i 's/"password": "password"/"password": "twadmin"/' /ThingworxPlatform/modelproviderconfig.json	
		sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform/modelproviderconfig.json	
	fi
else 
	echo "  ... Creating platform-settings.json"
	echo "{" > $TWXTEMP/platform-settings-gen.json
	echo "    \"PersistenceProviderPackageConfigs\": {" >> $TWXTEMP/platform-settings-gen.json
	echo "        \"PostgresPersistenceProviderPackage\": {" >> $TWXTEMP/platform-settings-gen.json
	echo "            \"ConnectionInformation\": {" >> $TWXTEMP/platform-settings-gen.json
	echo "                \"jdbcUrl\": \"jdbc:postgresql://localhost:5432/thingworx\"," >> $TWXTEMP/platform-settings-gen.json
	echo "                \"password\": \"twadmin\"," >> $TWXTEMP/platform-settings-gen.json
	echo "                \"username\": \"twadmin\"" >> $TWXTEMP/platform-settings-gen.json
	echo "            }" >> $TWXTEMP/platform-settings-gen.json
	echo "        }" >> $TWXTEMP/platform-settings-gen.json
	
	# Include licensing information for ThingWorx 8.1+ if provided.
	if [ ! -z "$SUPTUSER" -a ! -z "$SUPTPASS" ]; then

		# Common stanza
		echo "    }," >> $TWXTEMP/platform-settings-gen.json
		echo "    \"PlatformSettingsConfig\": {" >> $TWXTEMP/platform-settings-gen.json
		echo "        \"LicensingConnectionSettings\": {" >> $TWXTEMP/platform-settings-gen.json
		echo "            \"username\":\"$SUPTUSER\"," >> $TWXTEMP/platform-settings-gen.json
		echo "            \"password\":\"encrypt.licensing.password\"," >> $TWXTEMP/platform-settings-gen.json
		
		# TWX 8.1.0 and 8.1.1 require activation ID.
		if [ $TWXMIN -eq 1 -a $TWXSP -lt "3" ]; then 
			echo "            \"activationIds\":\"$ACTVID\"" >> $TWXTEMP/platform-settings-gen.json
		else
			# Assume 8.1.3+, which eliminates the activation ID, but now requires a 5 minute timeout to look up a license!
			echo "            \"timeout\":\"300\"" >> $TWXTEMP/platform-settings-gen.json
		fi
		
		# Common closure
		echo "        }" >> $TWXTEMP/platform-settings-gen.json
		echo "    }" >> $TWXTEMP/platform-settings-gen.json
		echo "}" >> $TWXTEMP/platform-settings-gen.json
		
		# Extract the WAR file
		echo "  ... Extracting ThingWorx.war for password encryption."
		unzip -d $TWXTEMP $TWXTEMP/Thingworx.war > /dev/null
		
		# Write out the password to the keystore.
		echo "  ... Encrypting eSupport password"
		sudo java -classpath "$TWXTEMP/WEB-INF/lib/*" com.thingworx.security.keystore.ThingworxKeyStore encrypt.licensing.password "$SUPTPASS"
		sudo chown $TOMCATUSR:$TOMCATGRP /ThingworxStorage/keystore.jks
		
	else
		
		# No licensing information, just close off the block.
		echo "    }" >> $TWXTEMP/platform-settings-gen.json
		echo "}" >> $TWXTEMP/platform-settings-gen.json
		
	fi

	# Copy generated file and change ownership to tomcat8.
	sudo cp $TWXTEMP/platform-settings-gen.json /ThingworxPlatform/platform-settings.json
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform/platform-settings.json	
	
fi

# If this is 7.4 or higher, copy the license.bin file to /ThingworxPlatform
if [ $TWXMAJ -ge 7 -a $TWXMIN -ge 4 ]; then 
	echo "  ... Copying license file to /ThingworxPlatform"
	sudo cp $TWXTEMP/license.bin /ThingworxPlatform
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform/license.bin
elif [ $TWXMAJ -ge 8 -a $TWXMIN -eq 0 ]; then 
	
	# 8.0 requires a license file that needs to be downloaded from the eSupport portal.
	# For ease of use, this file is included in the vagrant image, or can be retrieved from the vagrant folder for older images.
	LICENSEFILE="license-${TWXMAJ}.${TWXMIN}.bin"
	if [ -e "/vagrant/$LICENSEFILE" ]; then 
		# License found in vagrant folder.
		echo "  ... License found in /vagrant folder"
		LICENSEPATH="/vagrant/$LICENSEFILE"
	elif [ -e "/home/vagrant/licenses/$LICENSEFILE" ]; then 
		# License found in vagrant folder.
		echo "  ... License found in /home/vagrant/licenses folder"
		LICENSEPATH="/home/vagrant/licenses/$LICENSEFILE"	
	else
		echo "ERROR: Unable to find license file $LICENSEFILE in /vagrant or /home/vagrant/licenses!"
		exit 1
	fi
	
	echo "  ... Copying license file to /ThingworxPlatform"
	sudo cp $LICENSEPATH /ThingworxPlatform/license.bin
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform/license.bin
	
fi

# Restart Tomcat
sudo service tomcat restart

# If we're installing TWX 8.0, let's change the password back to "admin" to make it easier for us.
if [ $TWXMAJ -ge 8 ]; then 
	
	if [ $TWXMIN -eq 0 ]; then
		echo ""
		echo "Thingworx 8.x installation detected.  Resetting Administrator password."
		echo "  ... Waiting 30 seconds for ThingWorx server to start..."
		sleep 30
	else 
		# Thingworx 8.1.3+ requires up to 5 minutes to retrieve a license for a PTC account. 
		# This is "normal" and wont be changing, so we have to wait 5 minutes to be certain the server is up.
		echo ""
		echo "Thingworx 8.1+ installation detected.  Resetting Administrator password."
		echo "  ... Waiting 5 MINUTES for ThingWorx server to start..."
		sleep 300
	fi

	echo "  ... Attempting to change default password back to 'admin'"
	curl --max-time 60 -X POST http://localhost:8080/Thingworx/Users/Administrator/Services/ChangePassword -H 'accept: application/json' -H 'authorization: Basic QWRtaW5pc3RyYXRvcjp0clVmNnl1ejI/X0d1Yg==' -H 'content-type: application/json' -d '{"oldPassword":"trUf6yuz2?_Gub", "newPassword":"admin", "newPasswordConfirm":"admin"}'
	
	if [ $? -eq 0 ]; then
		echo "Password has been successfully changed."
	else
		echo "Request timed out. Administrator password may not have been changed."
	fi
fi

