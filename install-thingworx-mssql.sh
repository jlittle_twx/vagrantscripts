#!/bin/bash
# Usage: install-thingworx-mssql.sh [-t <tomcatVersion>] [-x <TWXVersion>] -h <SQLHost> [-p <SQLHost Port>] [-i <SQLinstanceName>] -a <SQLAdmin User> -b <SQLAdmin Password> [-d <databaseName>]
#   -x Thingworx Version (X.Y.Z) - Default '6.6.0'
# 	-t Tomcat Version - Either X.0.Y or X for latest from major version X. Default '8.0.33'
#	-h SQL Server hostname
#	-p SQL Server Port (Default 1433)
#	-i SQL Server Instance Name
#	-a SQL Admin User (Default 'sa')
#	-b SQL Admin Password
#	-d ThingWorx Database Name
#	-u eSupport Portal Username
#	-v eSupport Portal Password (will be encrypted)
#	-l License activation key (8.1)


# Default values
TOMCATVER="8"
TWXVER="7.4.0"
MSSQLPORT=1433
MSSQLADMINUSER=sa
TWADMINUSER=twadmin
TWADMINPASS=twadmin
MSSQLDBNAME=thingworx

# Constants
TWXTEMP="TWXExtract"
TOMCATUSR="tomcat8"
TOMCATDIR="/opt/tomcat8"

# Process flags as necessary.
while [ "$1" != "" ]; do
	case $1 in
		-x | -X )	shift
					TWXVER="$1"
					;;
		-t | -T )	shift
					TOMCATVER="$1"
					;;
		-h | -H )	shift
					MSSQLHOST="$1"
					;;
		-p | -P )	shift
					MSSQLPORT="$1"
					;;
		-i | -I )	shift
					MSSQLINSTANCE="$1"
					;;
		-d | -D )	shift
					MSSQLDBNAME="$1"
					;;
		-a | -A )	shift
					MSSQLADMINUSER="$1"
					;;
		-b | -B )	shift
					MSSQLADMINPASS="$1"
					;;
		-u | -U )	shift
					SUPTUSER="$1"
					;;
		-v | -V )	shift
					SUPTPASS="$1"
					;;
		-l | -L )	shift
					ACTVID="$1"
					;;					
		--help )	shift
					echo "usage: install-thingworx-mssql.sh [-t <tomcatVersion>] [-x <TWXVersion>] -h <SQLHost> [-p <SQLHost Port>] [-i <SQLinstanceName>] -a <SQLAdmin User> -b <SQLAdmin Password> [-d <databaseName>]"
					exit 1
					;;
		* )
					echo "Unknown Option $1"
					exit 1
					;;					
	esac
	shift
done

# Check mandatory parameters
if [ -z "$MSSQLHOST" ]; then
	echo "MSSQL Hostname (-h) must be specified."
	exit 1;
fi

if [ -z "$MSSQLADMINPASS" ]; then
	echo "MSSQL Administrator password (-b) must be specified. (using \"$MSSQLADMINUSER\")"
	exit 1;
fi

echo "Tomcat: $TOMCATVER, Thingworx: $TWXVER"
echo "MSSQL Server: $MSSQLHOST, MSSQL Port: $MSSQLPORT, MSSQL Instance: $MSSQLINSTANCE, MSSQL Admin: $MSSQLADMINUSER"

## VALIDATION
# Thingworx version - Only 7.4.0+ supported for MSSQL.
# For futureproofing, I'm only checking old versions.  New versions will attempt to download and fail if not available.
if [ `echo "$TWXVER" | cut -f1 -d.` -lt 7 ]; then 
	echo "Script only supports Thingworx 7.4.0 or higher. Thingworx Version provided: $TWXVER"
	exit 1
elif [ `echo "$TWXVER" | cut -f1 -d.` -eq 7 -a `echo "$TWXVER" | cut -f2 -d.` -lt 4 ]; then 
	echo "Script only supports Thingworx 7.4.0 or higher. Thingworx Version provided: $TWXVER"
	exit 1
fi

# Tomcat Version - Only 8.0 is supported.  Not checking minors.
if [ `echo "$TOMCATVER" | cut -f1 -d.` -ne 8 ]; then 
	echo "Script only supports Tomcat 7 or 8. Tomcat Version provided: $TOMCATVER"
	exit 1
fi

# Install Additional Packages if necessary
if ! dpkg -l unzip 2>&1 >/dev/null; then
	echo "  ... Installing unzip."
	sudo apt-get install -qy unzip >/dev/null
fi

if ! dpkg -l wget 2>&1 >/dev/null; then
	echo "  ... Installing wget."
	sudo apt-get install -qy wget >/dev/null
fi

# installs sqlcmd silently without license prompt
if ! dpkg -l mssql-tools 2>&1 >/dev/null; then
	echo "  ... installing mssql-tools."
	curl -s https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
	curl -s https://packages.microsoft.com/config/ubuntu/16.04/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list
	sudo apt-get update -qy
	sudo ACCEPT_EULA=y DEBIAN_FRONTEND=noninteractive apt-get install -qy mssql-tools unixodbc-dev >/dev/null

	# add sqlcmd to the path
	echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
	source ~/.bash_profile
	
fi

# Download Thingworx version
TWXMAJ=`echo $TWXVER | cut -f1 -d.`
TWXMIN=`echo $TWXVER | cut -f2 -d.`
TWXSP=`echo $TWXVER | cut -f3 -d.`

# eSupport Portal Information check
if [ $TWXMAJ -ge 8 -a $TWXMIN -ge 1 ]; then
	# ThingWorx 8.1 requires username and password for eSupport Portal to retrieve license.
	if [ -z "$SUPTUSER" -o -z "$SUPTPASS" ]; then 
		echo "ERROR: ThingWorx 8.1 requires a PTC username (-u) and password (-v) to be specified for licensing."
		exit 1
	fi
	
	# Pull in default activation ID if available
	if [ -z "$ACTVID" -a -e "/home/vagrant/licenses/activation.sh" ]; then 
		source /home/vagrant/licenses/activation.sh
	fi
	
	# Make sure an activation ID is specified
	if [ -z "$ACTVID" ]; then
		echo "ERROR: Activation ID is not specified.  Cannot continue!"
		exit 1
	fi 	
fi

echo "Downloading ThingWorx Platform $TWXVER for MSSQL"

# Generate the filename for the download based on PTC's archaic naming format.
if [ $TWXSP -eq 0 ]; then 
	# If no SP release, then we need to replace _SPx_ with _F000_
	TWXFILE="MED-61212-CD-0${TWXMAJ}${TWXMIN}_F000_ThingWorx-Platform-Mssql-${TWXMAJ}-${TWXMIN}-${TWXSP}.zip"
else 	
	TWXFILE="MED-61212-CD-0${TWXMAJ}${TWXMIN}_SP${TWXSP}_ThingWorx-Platform-Mssql-${TWXMAJ}-${TWXMIN}-${TWXSP}.zip"
fi

# Generate the URL
URL="https://esd.ptc.com/files/TWX/$TWXFILE"
	
if ! wget --quiet --spider --no-check-certificate $URL > /dev/null; then
	echo "  ...unable to find $TWXFILE.  Trying with 'Thingworx' instead"
	URL=${URL/ThingWorx/Thingworx}

	if ! wget --quiet --spider --no-check-certificate $URL > /dev/null; then
		echo "ERROR: Unable to download ThingWorx $TWXVER from $URL!"
		exit 1
	fi
fi

if [ -e "./$TWXFILE" ]; then
	# Thingworx file appears to have already been downloaded.
	echo "  ... Found $TWXFILE in local directory. Skipping download."
else 	
	# Download Thingworx from PTC, as spider check should have already been done
	echo "  ... Downloading Thingworx $TWXVER from PTC."
	wget --no-check-certificate $URL > /dev/null	
fi

# Download SQL Server driver from Microsoft
if [ ! -e "./sqljdbc_6.0.8112.100_enu.tar.gz" ]; then
	echo "Downloading SQL Server driver from Microsoft"
	wget https://download.microsoft.com/download/0/2/A/02AAE597-3865-456C-AE7F-613F99F850A8/enu/sqljdbc_6.0.8112.100_enu.tar.gz > /dev/null
fi

# Extract contents of zip
echo "Extracting ThingWorx to a Temporary Location"
if [ -d "$TWXTEMP" ]; then
	# Directory already exists - Remove and re-extract in case files have been removed/moved.	
	
	# Don't try to delete anything if $TWXTEMP variable is empty! (This could be very bad...)
	if [ ! -z "$TWXTEMP" ]; then 
		echo "  ... Temporary directory already exists. Removing existing files. "
		rm -rf "$TWXTEMP"
	fi
fi

# Extract the Thingworx download into a temporary directory.
echo "  ... Extracting $TWXFILE to temporary directory."
unzip "./$TWXFILE" -d "$TWXTEMP" > /dev/null

## PRE-REQUISITES ##

# Tomcat
# TODO: Script exits with code 1 if it finds an existing install
# Need better way to detect and skip installation.
echo "Installing Tomcat $TOMCATVER"
if [ -e "./tomcat-install.sh" ]; then
	if ! ./tomcat-install.sh $TOMCATVER; then
		echo "ERROR: Unable to install Tomcat!"
	fi
else 
	if ! $SCRIPTSDIR/tomcat-install.sh $TOMCATVER; then
		echo "ERROR: Unable to install Tomcat!"
	fi
fi

## SET UP MSSQL Database ##

# Create twadmin User if it has not already been created.
sqlcmd -S $MSSQLHOST -U $MSSQLADMINUSER -P $MSSQLADMINPASS -Q "if not exists (select loginname from master.dbo.syslogins where name = '$TWADMINUSER') Begin CREATE LOGIN $TWADMINUSER WITH PASSWORD = '$TWADMINPASS' End"

# Run thingworxMssqlDBSetup.sh script

CWD=`pwd`
	
if [ -d $TWXTEMP/install ]; then
	cd $TWXTEMP/install/
else
	cd $TWXTEMP
fi
	
# Can't pass in any null parameters.  Instance might be empty, so pull the parameter/switch out if not specified.	
if [ -z $MSSQLINSTANCE ]; then 
	
	# No instance specified
	echo "  ... Running thingworxMssqlDBSetup Script"
	./thingworxMssqlDBSetup.sh -h $MSSQLHOST -p $MSSQLPORT -a $MSSQLADMINUSER -r $MSSQLADMINPASS -l $TWADMINUSER -d $MSSQLDBNAME -u $TWADMINUSER
	if [ $? -ne 0 ]; then 
		echo "ERROR: Unable to run thingworxMssqlDBSetup Script! Exiting."
		exit 1
	fi

	echo "  ... Running thingworxMssqlSchemaSetup"
	./thingworxMssqlSchemaSetup.sh -h $MSSQLHOST -p $MSSQLPORT -r $TWADMINPASS -l $TWADMINUSER -d $MSSQLDBNAME
	if [ $? -ne 0 ]; then 
		echo "ERROR: Unable to run thingworxMssqlSchemaSetup Script! Exiting."
		exit 1
	fi	
else 
	# Instance was specified
	echo "  ... Running thingworxMssqlDBSetup Script with Instance"
	if ! ./thingworxMssqlDBSetup.sh -h $MSSQLHOST -p $MSSQLPORT -i $MSSQLINSTANCE -a $MSSQLADMINUSER -r $MSSQLADMINPASS -l $TWADMINUSER -d $MSSQLDBNAME -u $TWADMINUSER; then 
		echo "ERROR: Unable to run thingworxMssqlDBSetup Script! Exiting."
		exit 1
	fi

	echo "  ... Running thingworxMssqlSchemaSetup with Instance"
	if ! ./thingworxMssqlSchemaSetup.sh -h $MSSQLHOST -p $MSSQLPORT -i $MSSQLINSTANCE -r $TWADMINPASS -l $TWADMINUSER -d $MSSQLDBNAME; then 
		echo "ERROR: Unable to run thingworxMssqlSchemaSetup Script! Exiting."
		exit 1
	fi
fi
	
	
cd $CWD	


## INSTALL THINGWORX ##

# Create /ThingworxPlatform directory and set ownership to Tomcat
if [ ! -d "/ThingworxPlatform" ]; then 
	echo "  ... Creating /ThingworxPlatform directory"
	sudo mkdir /ThingworxPlatform
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform
fi

# Create /ThingworxStorage directory and set ownership to Tomcat
if [ ! -d "/ThingworxStorage" ]; then 
	echo "  ... Creating /ThingworxStorage directory"
	sudo mkdir /ThingworxStorage
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxStorage
fi

# Create /ThingworxBackupStorage directory and set ownership to Tomcat
if [ ! -d "/ThingworxBackupStorage" ]; then 
	echo "  ... Creating /ThingworxBackupStorage directory"
	sudo mkdir /ThingworxBackupStorage
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxBackupStorage
fi


echo "  ... Creating platform-settings.json"
echo "{" > $TWXTEMP/platform-settings-gen.json
echo "    \"PersistenceProviderPackageConfigs\": {" >> $TWXTEMP/platform-settings-gen.json
echo "        \"MssqlPersistenceProviderPackage\": {" >> $TWXTEMP/platform-settings-gen.json
echo "            \"ConnectionInformation\": {" >> $TWXTEMP/platform-settings-gen.json
echo "                \"driverClass\": \"com.microsoft.sqlserver.jdbc.SQLServerDriver\"," >> $TWXTEMP/platform-settings-gen.json
				
if [ -z $MSSQLINSTANCE ]; then 
	echo "                \"jdbcUrl\": \"jdbc:sqlserver://$MSSQLHOST:$MSSQLPORT;databaseName=$MSSQLDBNAME;applicationName=Thingworx;\"," >> $TWXTEMP/platform-settings-gen.json
else
	echo "                \"jdbcUrl\": \"jdbc:sqlserver://$MSSQLHOST:$MSSQLPORT;instanceName=$MSSQLINSTANCE;databaseName=$MSSQLDBNAME;applicationName=Thingworx;\"," >> $TWXTEMP/platform-settings-gen.json
fi

echo "                \"password\": \"$TWADMINUSER\"," >> $TWXTEMP/platform-settings-gen.json
echo "                \"username\": \"$TWADMINPASS\"" >> $TWXTEMP/platform-settings-gen.json
echo "            }" >> $TWXTEMP/platform-settings-gen.json
echo "        }" >> $TWXTEMP/platform-settings-gen.json
	
# Include licensing information for ThingWorx 8.1 if provided.
if [ ! -z "$SUPTUSER" -a ! -z "$SUPTPASS" -a ! -z "$ACTVID" ]; then
	echo "    }," >> $TWXTEMP/platform-settings-gen.json
	echo "    \"PlatformSettingsConfig\": {" >> $TWXTEMP/platform-settings-gen.json
	echo "        \"LicensingConnectionSettings\": {" >> $TWXTEMP/platform-settings-gen.json
	echo "            \"username\":\"$SUPTUSER\"," >> $TWXTEMP/platform-settings-gen.json
	echo "            \"password\":\"encrypt.licensing.password\"," >> $TWXTEMP/platform-settings-gen.json
	echo "            \"activationIds\":\"$ACTVID\"" >> $TWXTEMP/platform-settings-gen.json
	echo "        }" >> $TWXTEMP/platform-settings-gen.json
	echo "    }" >> $TWXTEMP/platform-settings-gen.json
	echo "}" >> $TWXTEMP/platform-settings-gen.json

	# Extract the WAR file
	echo "  ... Extracting ThingWorx.war for password encryption."
	unzip -d $TWXTEMP $TWXTEMP/Thingworx.war > /dev/null
		
	# Write out the password to the keystore.
	echo "  ... Encrypting eSupport password"
	sudo java -classpath "$TWXTEMP/WEB-INF/lib/*" com.thingworx.security.keystore.ThingworxKeyStore encrypt.licensing.password "$SUPTPASS"
	sudo chown $TOMCATUSR:$TOMCATGRP /ThingworxStorage/keystore.jks
		
else
		
	# No licensing information, just close off the block.
	echo "    }" >> $TWXTEMP/platform-settings-gen.json
	echo "}" >> $TWXTEMP/platform-settings-gen.json
		
fi

# Copy generated platform-settings file and change ownership to tomcat8.
sudo cp $TWXTEMP/platform-settings-gen.json /ThingworxPlatform/platform-settings.json
sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform/platform-settings.json	

# If this is 7.4 or higher, copy the license.bin file to /ThingworxPlatform
if [ $TWXMAJ -ge 7 -a $TWXMIN -ge 4 ]; then 
	echo "  ... Copying license file to /ThingworxPlatform"
	sudo cp $TWXTEMP/license.bin /ThingworxPlatform
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform/license.bin
elif [ $TWXMAJ -eq 8 -a $TWXMIN -eq 0 ]; then 
	LICENSEFILE="license-${TWXMAJ}.${TWXMIN}.bin"
	if [ -e "/vagrant/$LICENSEFILE" ]; then 
		# License found in vagrant folder.
		echo "  ... License found in /vagrant folder"
		LICENSEPATH="/vagrant/$LICENSEFILE"
	elif [ -e "/home/vagrant/licenses/$LICENSEFILE" ]; then 
		# License found in vagrant folder.
		echo "  ... License found in /home/vagrant/licenses folder"
		LICENSEPATH="/home/vagrant/licenses/$LICENSEFILE"	
	else
		echo "ERROR: Unable to find license file $LICENSEFILE in /vagrant or /home/vagrant/licenses!"
		exit 1
	fi
	
	echo "  ... Copying license file to /ThingworxPlatform"
	sudo cp $LICENSEPATH /ThingworxPlatform/license.bin
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform/license.bin
	
fi

# Copy Thingworx.war to Tomcat webapps directory
if [ ! -e "$TOMCATDIR/webapps/Thingworx.war" ]; then 
	echo "  ... copying Thingworx.war"
	sudo cp $TWXTEMP/Thingworx.war $TOMCATDIR/webapps
	sudo chown $TOMCATUSR:$TOMCATUSR $TOMCATDIR/webapps/Thingworx.war
fi

# Unzip SQL driver
tar -xf sqljdbc_6.0.8112.100_enu.tar.gz

# Copy SQL Driver to ThingWorx installation
sudo cp sqljdbc_6.0/enu/jre8/sqljdbc42.jar /opt/tomcat8/lib
sudo chown tomcat8:tomcat8 /opt/tomcat8/lib/sqljdbc42.jar

# Restart Tomcat
sudo service tomcat restart

# If we're installing TWX 8.0, let's change the password back to "admin" to make it easier for us.
if [ $TWXMAJ -ge 8 ]; then 
	echo ""
	echo "Thingworx 8.x installation detected.  Resetting Administrator password."
	echo "  ... Waiting 30 seconds for ThingWorx server to start..."
	sleep 30

	echo "  ... Attempting to change default password back to 'admin'"
	curl --max-time 60 -X POST http://localhost:8080/Thingworx/Users/Administrator/Services/ChangePassword -H 'accept: application/json' -H 'authorization: Basic QWRtaW5pc3RyYXRvcjp0clVmNnl1ejI/X0d1Yg==' -H 'content-type: application/json' -d '{"oldPassword":"trUf6yuz2?_Gub", "newPassword":"admin", "newPasswordConfirm":"admin"}'
	
	if [ $? -eq 0 ]; then
		echo "Password has been successfully changed."
	else
		echo "Request timed out. Administrator password may not have been changed."
	fi
fi
