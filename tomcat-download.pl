#!/usr/bin/perl
# tomcat-download.pl <major> or <major.0.minor>
# Downloads a specific version of Apache Tomcat, and writes the filename to last_downloaded.txt

# To implement in later versions:
#    - download to a specific location and only download if not already present.
#    - Add mirror support to try multiple sites in case main site goes down.

use strict;
use warnings;
use Mojo;

# Check Parameters
(scalar @ARGV) == 1 or die("Expecting one parameter.");
my ($version) = @ARGV;

# Matches major version only or major.0.minor.  
# If minor is not specified, we'll download latest.
if ($version !~ /[6-8](\.0\.[0-9]+)?/) {
	die ("Only supports 6, 7, or 8 streams.")
}

# Split out the major and minor version.
my ($major, $dummy, $minor) = split ('\.', $version);

#Debugging
#if ($minor) {
#	print "Major: $major, minor: $minor\n";
#} else {
#	print "Major: $major";
#}
	
# Download Tomcat
my @mirrors = ("http://ftp.wayne.edu/apache/tomcat/");
my $archive = ("http://archive.apache.org/dist/tomcat/");

# This code should be updated at some point to cycle through a mirrors list in the event that one is not responding.
my $url = $mirrors[0]."tomcat-$major/";

# Create a user agent to perform the HTTP request.
my $ua = Mojo::UserAgent->new; 

# Use the User Agent to get a directory listing from the URL.
# res -> Response object from HTTP Server
# dom -> DOM object from response
# find -> searches for anchor "a" tags.
my $resp = $ua->get($url)->res->dom->find("a")->to_array;

if ($minor) {
	# If $minor is defined, we attempt to find a folder with the proper major.0.minor format.
	if (grep { /v${major}.0.${minor}\// } @{$resp}) {
		# There is a corresponding folder in the target directory. Update the URL.
		print "Found v$major.0.$minor.\n";
		$url .= "v$major.0.$minor/bin/apache-tomcat-$major.0.$minor.tar.gz";
	} else {
		print "Did not find v$major.0.$minor. Trying the archive.\n";
		my $archiveURL = $archive."tomcat-$major/";
		my $resp2 = $ua->get($archiveURL)->res->dom->find("a")->to_array;
		
		if (grep { /v${major}.0.${minor}\// } @{$resp2}) {
			print "Found v$major.0.$minor in the archive.\n";
			$url = $archiveURL."v$major.0.$minor/bin/apache-tomcat-$major.0.$minor.tar.gz";
		} else {
			print "Did not find v$major.0.$minor in the archive either. Changing URL to pull latest from Tomcat $major.\n";
			$minor = undef;
		}
	}
	
}

if (!$minor) {
	# If $minor is not defined at this point, we just want to grab the latest version.
	my @result;
	foreach (@{$resp}) {
		if ( $_->text =~ /v$major\.0\.[0-9]+\//) {
			push @result, $_->text; 
		}
	}
	
	# Determine which version is latest
	$minor = 0;
	foreach (@result){
		my ($m1, $m2, $m3) = split ('\.', $_);
		($m3) = split ("/", $m3);
		$minor = $m3 if $m3 > $minor;
	}
	
	# Update the URL to include the actual filename we want to download.
	$url .= "v$major.0.$minor/bin/apache-tomcat-$major.0.$minor.tar.gz";
}

# Download the file.
print "Downloading Apache Tomcat v$major.0.$minor from: $url\n";

# wget options used: 
# -nc - No clobber (will not download file if it already exists)
# --timeout - set timeout to more reasonable 2 minutes rather than 900 second default
system ("wget", "-nc", "--timeout=120", $url) == 0 or die("Unable to call wget");

# Write out the filename that we just downloaded for other scripts to access.
my $fn = "last_downloaded.txt";
open(my $fh, '>', $fn ) or die ("Unable to open $fn for write.");
$url =~ /(apache-tomcat.*)/;
print $fh $1;
close ($fh);