#!/bin/sh
#

# Variables
TOMCATUSR="tomcat8"
TOMCATGRP="tomcat8"
TOMCATDIR="/opt/tomcat8"
JAVA_START_OPTS="-Xms512m -Xmx2048m -server -d64 -XX:+UseG1GC -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true -Djava.library.path=\$CATALINA_HOME/webapps/Thingworx/WEB-INF/extensions"
TOMCAT_OPTS="-Dfile.encoding=UTF-8 -Xloggc:\$CATALINA_HOME/logs/gc.out -XX:+PrintGCTimeStamps -XX:+PrintGCDetails -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/ThingworxStorage/heapdump.hprof"

# Check parameters
if [ $# -ne 1 ]; then
	echo "Usage: tomcat-install.sh <version>"
	echo "   where <version> is either a major version (6-8) or a full version string (8.0.30)"
	exit 1
fi

if ! echo "$1" | grep -Eq "[6-8](\.0\.[0-9]+)?"; then 
	echo "Version $1 is not valid."
	echo "Usage: tomcat-install.sh <version>"
	echo "   where <version> is either a major version (6-8) or a full version string (8.0.30)"
	exit 1
fi	

# Determine distro
if cat /proc/version | grep -qi "Ubuntu"; then
	echo "Installing Tomcat for Ubuntu..."
	UBUNTU=1
	UBUNTUVER=`lsb_release -r | cut -f2`
	MAJVER=`echo $UBUNTUVER | cut -f1 -d.`
	MINVER=`echo $UBUNTUVER | cut -f2 -d.`
elif cat /proc/version | grep -qi "Red Hat"; then
	if cat /etc/redhat-release | grep -qi "CentOS"; then 
		echo "Installing Tomcat for CentOS"
		CENTOS=1
	else
		echo "Installing Tomcat for Red Hat"
		REDHAT=1
	fi
fi

# Install Mojolicious into perl if necessary.  It's required for the tomcat-download script
if ! perl -MMojo -e 1 2>&1 >/dev/null; then
	echo "  ... Installing Mojolicious."
	
	if [ "$CENTOS" = "1" ]; then 
		# Need to do some prerequisites on CentOS
		# TODO: Does this need to be done for RHEL?
		echo "  ... CentOS detected.  Installing Prerequisites."
		sudo yum install -y cpan perl-Digest-MD5 perl-Compress-Raw-Zlib
	fi
	
	curl -L https://cpanmin.us | sudo perl - -M https://cpan.metacpan.org -n Mojolicious >/dev/null
fi

# Download Tomcat if necessary
# TODO: Better detect existing download. Currently always downloads if provided with Major only.
if [ ! -e "apache-tomcat-$1.tar.gz" ]; then
	echo "  ... Downloading Apache Tomcat v$1"
	if ! perl /home/vagrant/scripts/tomcat-download.pl $1; then 
		echo "Unable to download Tomcat!"
		exit 1
	fi
	TOMCATDL=`cat last_downloaded.txt`
else 
	TOMCATDL="apache-tomcat-$1.tar.gz"
fi

# Install prerequisites

# NTP
# Keeps clocks in sync.
if [ "$UBUNTU" = "1" ]; then 
	if ! dpkg -l ntp 2>&1 >/dev/null; then
		echo "  ... Installing NTP."
		sudo apt-get install -qy ntp
	fi
elif [ "$CENTOS" = "1" -o "$REDHAT" = "1" ]; then
	if ! yum list installed ntp; then 
		echo "  ... Installing NTP."
		sudo yum install -y ntp
	fi
fi

# Create the Tomcat user/group
if ! getent group $TOMCATGRP >/dev/null; then 
	echo "  ... Creating tomcat group: $TOMCATGRP"
	sudo groupadd $TOMCATGRP
fi

if ! getent passwd $TOMCATUSR >/dev/null; then 
	echo "  ... Creating tomcat user: $TOMCATUSR"
	sudo useradd -s /bin/false -g $TOMCATGRP -d $TOMCATDIR $TOMCATUSR
fi

# Installing Tomcat
if [ ! -d "$TOMCATDIR" ]; then
	echo "  ... Creating Tomcat Installation directory."
	sudo mkdir -p $TOMCATDIR
fi

if sudo find $TOMCATDIR/ -maxdepth 0 -empty | read v; then
	echo "  ... Extracting Tomcat and copying to $TOMCATDIR"
	sudo tar -zxf $TOMCATDL -C $TOMCATDIR --strip-components=1

	# User Permissions
	sudo chgrp -R $TOMCATGRP $TOMCATDIR/conf
	sudo chmod g+rwx $TOMCATDIR/conf
	sudo chmod g+r $TOMCATDIR/conf/*
	sudo chown -R $TOMCATUSR:$TOMCATGRP $TOMCATDIR/work/ $TOMCATDIR/temp/ $TOMCATDIR/logs/ $TOMCATDIR/webapps
	
else 
	if [ -e "$TOMCATDIR/bin/catalina.sh" ]; then 
		echo "WARNING: An existing installation was detected at $TOMCATDIR. Assuming installed."
		exit 0
	else
		echo "ERROR: $TOMCATDIR is not empty, but no sign of startup script. Assuming there is an error."
		exit 1
	fi
fi

# Set Up Ubuntu
if [ "$UBUNTU" = "1" ]; then
	if [ ! -f /etc/systemd/system/tomcat.service ]; then 

		# Set up Ubuntu using systemd
		echo "Configuring systemd... (Ubuntu 15 or newer detected)"
		
		echo "# Systemd unit file for tomcat" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "[Unit]" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Description=Apache Tomcat Web Application Container" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "After=syslog.target network.target" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "[Service]" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Type=forking" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment=JAVA_HOME=/usr/lib/jvm/java-8-oracle/jre" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment=CATALINA_PID=$TOMCATDIR/temp/tomcat.pid" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment=CATALINA_HOME=$TOMCATDIR" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment=CATALINA_BASE=$TOMCATDIR" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "# JVM Options" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment='CATALINA_OPTS=$TOMCAT_OPTS'" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment='JAVA_OPTS=$JAVA_START_OPTS'" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "ExecStart=$TOMCATDIR/bin/startup.sh" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "ExecStop=$TOMCATDIR/bin/shutdown.sh" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "# The user/group here should match the user configured for Tomcat." | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "User=tomcat8" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Group=tomcat8" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "[Install]" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "WantedBy=multi-user.target" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
			
		# validate the file
		sudo systemd-analyze verify tomcat.service
			
		# Reload configuration files
		sudo systemctl daemon-reload
		sudo systemctl enable tomcat
	fi
elif [ "$CENTOS" = "1" ]; then 
	# Set up systemd (CentOS)
	if [ ! -f /etc/systemd/system/tomcat.service ]; then 
		echo "# Systemd unit file for tomcat" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "[Unit]" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Description=Apache Tomcat Web Application Container" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "After=syslog.target network.target" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "[Service]" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Type=forking" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment=JAVA_HOME=/usr/java/jdk1.8.0_66/jre" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment=CATALINA_PID=$TOMCATDIR/temp/tomcat.pid" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment=CATALINA_HOME=$TOMCATDIR" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment=CATALINA_BASE=$TOMCATDIR" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment='CATALINA_OPTS=$TOMCAT_OPTS'" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "ExecStart=$TOMCATDIR/bin/startup.sh" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "ExecStop=/bin/kill -15 \$MAINPID" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "User=$TOMCATUSR" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "Group=$TOMCATGRP" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "[Install]" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		echo "WantedBy=multi-user.target" | sudo tee -a /etc/systemd/system/tomcat.service >/dev/null
		
		# Reload systemctl
		sudo systemctl daemon-reload
		
		# Set for autostart
		sudo systemctl enable tomcat 
	fi
fi

# Set up tomcat management users
sudo cp $TOMCATDIR/conf/tomcat-users.xml $TOMCATDIR/conf/tomcat-users.xml.bak
sudo sed -i '/<\/tomcat-users>/i   <user username="admin" password="admin" roles="manager-gui,admin-gui"/>' $TOMCATDIR/conf/tomcat-users.xml

# Change ownership of /opt/tomcat8 to the tomcat user.
sudo chown $TOMCATUSR:$TOMCATGRP $TOMCATDIR


# Start the server
if [ "$CENTOS" = "1" -o $MAJVER -gt 14 ]; then 
	# Start service - systemd (Centos, Ubuntu 15+)
	sudo systemctl start tomcat
elif [ "$UBUNTU" = "1" ]; then 
	sudo initctl start tomcat
fi

