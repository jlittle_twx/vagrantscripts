#!/bin/bash
# Usage: ./install-thingworx-neo4j.sh [-t <tomcatVersion>] [-x <thingworxVersion> | -f <thingworxZipFile>]
#   -x Thingworx Version (X.Y.Z) - Default '6.6.0'
#		OR
#	-f ThingWorx Zip File (for pre-release or custom built versions)
# 	-t Tomcat Version - Either X.0.Y or X for latest from major version X. Default '8'
#	-u eSupport Portal Username
#	-v eSupport Portal Password (will be encrypted)
#	-l License activation key (8.1)

# Default values
TOMCATVER="8"
TWXVER="7.2.0"

# Constants
TWXTEMP="TWXExtract"
TOMCATUSR="tomcat8"
TOMCATDIR="/opt/tomcat8"

# Process flags as necessary.
while [ "$1" != "" ]; do
	case $1 in
		-x | -X )	shift
					TWXVER="$1"
					if [ -z "$SRC" ]; then 
						SRC="X"
					else
						echo "ERROR - Cannot specify both the -x and -f flags at the same time!"
						exit 1
					fi
					;;
		-t | -T )	shift
					TOMCATVER="$1"
					;;
		-f | -F )	shift
					TWXFILE="$1"
					if [ -z "$SRC" ]; then 
						SRC="F"
					else
						echo "ERROR - Cannot specify both the -x and -f flags at the same time!"
						exit 1
					fi
					;;
		-u | -U )	shift
					SUPTUSER="$1"
					;;
		-v | -V )	shift
					SUPTPASS="$1"
					;;
		-l | -L )	shift
					ACTVID="$1"
					;;
		--help )	shift
					echo "usage: install-thingworx-h2.sh [-t <tomcatVersion>] [-x <thingworxVersion> | -f <ThingWorxZipFile>] [-u eSupport username] [-v eSupport Password] [-l TWX 8.1+ Activation ID]"
					exit 1
					;;
		* )
					echo "Unknown Option $1"
					exit 1
					;;
	esac
	shift
done

if [ $SRC == "F" ]; then 
	echo "Tomcat: $TOMCATVER, Thingworx File: $TWXFILE"
else
	echo "Tomcat: $TOMCATVER, Thingworx: $TWXVER"
fi

## VALIDATION
# Thingworx version - Only 7.2+ supported
# For futureproofing, I'm only checking old versions.  New versions will attempt to download and fail if not available.
if [ `echo "$TWXVER" | cut -f1 -d.` -lt 7 ]; then 
	echo "Script only supports Thingworx 7.2.0 or higher. Thingworx Version provided: $TWXVER"
	exit 1
else
	if [ `echo "$TWXVER" | cut -f1 -d.` -eq 7 -a `echo "$TWXVER" | cut -f2 -d.` -lt 2 ]; then
		echo "Script only supports Thingworx 7.2.0 or higher. Thingworx Version provided: $TWXVER"
		exit 1
	fi
fi

# Tomcat Version - Only 7 and 8 are supported.  Not checking minors.
if [ `echo "$TOMCATVER" | cut -f1 -d.` -ne 7 -a `echo "$TOMCATVER" | cut -f1 -d.` -ne 8 ]; then 
	echo "Script only supports Tomcat 7 or 8. Tomcat Version provided: $TOMCATVER"
	exit 1
fi

# Install Additional Packages if necessary
if ! dpkg -l unzip 2>&1 >/dev/null; then
	echo "  ... Installing unzip."
	sudo apt-get install -qy unzip >/dev/null
fi

if ! dpkg -l wget 2>&1 >/dev/null; then
	echo "  ... Installing wget."
	sudo apt-get install -qy wget >/dev/null
fi

# Download Thingworx version
TWXMAJ=`echo $TWXVER | cut -f1 -d.`
TWXMIN=`echo $TWXVER | cut -f2 -d.`
TWXSP=`echo $TWXVER | cut -f3 -d.`

# eSupport Portal Information check
if [ $TWXMAJ -ge 8 -a $TWXMIN -ge 1 ]; then
	# ThingWorx 8.1 requires username and password for eSupport Portal to retrieve license.
	if [ -z "$SUPTUSER" -o -z "$SUPTPASS" ]; then 
		echo "ERROR: ThingWorx 8.1 requires a PTC username (-u) and password (-v) to be specified for licensing."
		exit 1
	fi
	
	# Pull in default activation ID if available
	if [ -z "$ACTVID" -a -e "/home/vagrant/licenses/activation.sh" ]; then 
		source /home/vagrant/licenses/activation.sh
	fi
	
	# Make sure an activation ID is specified
	if [ -z "$ACTVID" ]; then
		echo "ERROR: Activation ID is not specified.  Cannot continue!"
		exit 1
	fi 	
fi

if [ -z "$TWXFILE" ]; then 
	echo "Downloading ThingWorx Platform $TWXVER for H2"

	# Generate the filename for the download based on PTC's archaic naming format.
	if [ $TWXSP -eq 0 ]; then 
		# If no SP release, then we need to replace _SPx_ with _F000_
		TWXFILE="MED-61182-CD-0${TWXMAJ}${TWXMIN}_F000_ThingWorx-Platform-H2-${TWXMAJ}-${TWXMIN}-${TWXSP}.zip"
	else 
		TWXFILE="MED-61182-CD-0${TWXMAJ}${TWXMIN}_SP${TWXSP}_ThingWorx-Platform-H2-${TWXMAJ}-${TWXMIN}-${TWXSP}.zip"
	fi	

	# Generate the URL
	URL="https://esd.ptc.com/files/TWX/$TWXFILE"

	if [ -e "./$TWXFILE" ]; then
		# Thingworx file appears to have already been downloaded.
		echo "  ... Found $TWXFILE in local directory. Skipping download."
	else 
		if ! wget --quiet --spider --no-check-certificate $URL > /dev/null; then
			echo "ERROR: Unable to download ThingWorx $TWXVER from $URL!"
			exit 1
		fi
	
		# Download Thingworx from PTC
		echo "  ... Downloading Thingworx $TWXVER from PTC."
		wget --no-check-certificate $URL > /dev/null
	fi
fi

# Extract contents of zip
echo "Extracting ThingWorx to a Temporary Location"
if [ -d "$TWXTEMP" ]; then
	# Directory already exists - Remove and re-extract in case files have been removed/moved.	
	
	# Don't try to delete anything if $TWXTEMP variable is empty! (This could be very bad...)
	if [ ! -z "$TWXTEMP" ]; then 
		echo "  ... Temporary directory already exists. Removing existing files. "
		rm -rf "$TWXTEMP"
	fi
fi

# Extract the Thingworx download into a temporary directory.
echo "  ... Extracting $TWXFILE to temporary directory."
unzip "./$TWXFILE" -d "$TWXTEMP" > /dev/null

## PRE-REQUISITES ##

# Tomcat
# TODO: Script exits with code 1 if it finds an existing install
# Need better way to detect and skip installation.
echo "Installing Tomcat $TOMCATVER"
if [ -e "./tomcat-install.sh" ]; then
	if ! ./tomcat-install.sh $TOMCATVER; then
		echo "ERROR: Unable to install Tomcat!"
	fi
else 
	if ! $SCRIPTSDIR/tomcat-install.sh $TOMCATVER; then
		echo "ERROR: Unable to install Tomcat!"
	fi
fi

## INSTALL THINGWORX ##

# Create /ThingworxPlatform directory and set ownership to Tomcat
if [ ! -d "/ThingworxPlatform" ]; then 
	echo "  ... Creating /ThingworxPlatform directory"
	sudo mkdir /ThingworxPlatform
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform
fi

# Create /ThingworxStorage directory and set ownership to Tomcat
if [ ! -d "/ThingworxStorage" ]; then 
	echo "  ... Creating /ThingworxStorage directory"
	sudo mkdir /ThingworxStorage
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxStorage
fi

# Create /ThingworxBackupStorage directory and set ownership to Tomcat
if [ ! -d "/ThingworxBackupStorage" ]; then 
	echo "  ... Creating /ThingworxBackupStorage directory"
	sudo mkdir /ThingworxBackupStorage
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxBackupStorage
fi


# If this is 7.4 or higher, copy the license.bin file to /ThingworxPlatform
if [ $TWXMAJ -ge 7 -a $TWXMIN -ge 4 ]; then 
	echo "  ... Copying license file to /ThingworxPlatform"
	sudo cp $TWXTEMP/license.bin /ThingworxPlatform
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform/license.bin
elif [ $TWXMAJ -eq 8 -a $TWXMIN -eq 0 ]; then 
	LICENSEFILE="license-${TWXMAJ}.${TWXMIN}.bin"
	if [ -e "/vagrant/$LICENSEFILE" ]; then 
		# License found in vagrant folder.
		echo "  ... License found in /vagrant folder"
		LICENSEPATH="/vagrant/$LICENSEFILE"
	elif [ -e "/home/vagrant/licenses/$LICENSEFILE" ]; then 
		# License found in vagrant folder.
		echo "  ... License found in /home/vagrant/licenses folder"
		LICENSEPATH="/home/vagrant/licenses/$LICENSEFILE"	
	else
		echo "ERROR: Unable to find license file $LICENSEFILE in /vagrant or /home/vagrant/licenses!"
		exit 1
	fi
	
	echo "  ... Copying license file to /ThingworxPlatform"
	sudo cp $LICENSEPATH /ThingworxPlatform/license.bin
	sudo chown $TOMCATUSR:$TOMCATUSR /ThingworxPlatform/license.bin
	
else 
	
	# Include licensing information for ThingWorx 8.1 if provided.
	if [ ! -z "$SUPTUSER" -a ! -z "$SUPTPASS" -a ! -z "$ACTVID" ]; then

		echo "  ... Creating platform-settings.json"
		echo "{" > $TWXTEMP/platform-settings-gen.json
		echo "    \"PlatformSettingsConfig\": {" >> $TWXTEMP/platform-settings-gen.json
		echo "        \"LicensingConnectionSettings\": {" >> $TWXTEMP/platform-settings-gen.json
		echo "            \"username\":\"$SUPTUSER\"," >> $TWXTEMP/platform-settings-gen.json
		echo "            \"password\":\"encrypt.licensing.password\"," >> $TWXTEMP/platform-settings-gen.json
		echo "            \"activationIds\":\"$ACTVID\"" >> $TWXTEMP/platform-settings-gen.json
		echo "        }" >> $TWXTEMP/platform-settings-gen.json
		echo "    }" >> $TWXTEMP/platform-settings-gen.json
		echo "}" >> $TWXTEMP/platform-settings-gen.json

		sudo cp $TWXTEMP/platform-settings-gen.json /ThingworxPlatform/platform-settings.json
		sudo chown $TOMCATUSR:$TOMCATGRP /ThingworxPlatform/platform-settings.json

		# Extract the WAR file
		echo "  ... Extracting ThingWorx.war for password encryption."
		unzip -d $TWXTEMP $TWXTEMP/Thingworx.war > /dev/null
		
		# Write out the password to the keystore.
		echo "  ... Encrypting eSupport password"
		sudo java -classpath "$TWXTEMP/WEB-INF/lib/*" com.thingworx.security.keystore.ThingworxKeyStore encrypt.licensing.password "$SUPTPASS"
		sudo chown $TOMCATUSR:$TOMCATGRP /ThingworxStorage/keystore.jks
		
	fi

fi

# Copy Thingworx.war to Tomcat webapps directory
if [ ! -e "$TOMCATDIR/webapps/Thingworx.war" ]; then 
	echo "  ... copying Thingworx.war"
	if [ -e "$TWXTEMP/Thingworx.war" ]; then 
		sudo cp $TWXTEMP/Thingworx.war $TOMCATDIR/webapps
		sudo chown $TOMCATUSR:$TOMCATUSR $TOMCATDIR/webapps/Thingworx.war
	fi
fi

# Restart Tomcat
sudo service tomcat restart

# If we're installing TWX 8.0, let's change the password back to "admin" to make it easier for us.
if [ $TWXMAJ -ge 8 ]; then 
	echo ""
	echo "Thingworx 8.x installation detected.  Resetting Administrator password."
	echo "  ... Waiting 30 seconds for ThingWorx server to start..."
	sleep 30

	echo "  ... Attempting to change default password back to 'admin'"
	curl --max-time 60 -X POST http://localhost:8080/Thingworx/Users/Administrator/Services/ChangePassword -H 'accept: application/json' -H 'authorization: Basic QWRtaW5pc3RyYXRvcjp0clVmNnl1ejI/X0d1Yg==' -H 'content-type: application/json' -d '{"oldPassword":"trUf6yuz2?_Gub", "newPassword":"admin", "newPasswordConfirm":"admin"}'
	
	if [ $? -eq 0 ]; then
		echo "Password has been successfully changed."
	else
		echo "Request timed out. Administrator password may not have been changed."
	fi
fi
